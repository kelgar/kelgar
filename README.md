# README #

Creating the wonderful world of Kelgar

### How do I get set up? ###

Please refer to this guide:
http://kelgardev.forumieren.org/t144-how-to-contribute-changes#3098

### Where can I find the TODO? ###

Click on Issues or follow this link:
https://bitbucket.org/kelgar/kelgar/issues

### Where do I see what has been contributed? ###

Click on Commits or follow this link:
https://bitbucket.org/kelgar/kelgar/commits
